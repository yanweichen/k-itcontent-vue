
import Vue from 'vue'
import Router from 'vue-router'
import vueSource from 'vue-resource'

import App from './App'

import routers from './router'

Vue.use(Router)
Vue.use(vueSource)

var router = new Router()

routers(router);

router.start(App, '#app')
