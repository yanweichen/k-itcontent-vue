module.exports = function (router) {
    router.map({
        '/default': {
        name: 'default',
        component: require('./components/home.vue')
      },
      '/header': {
        name: 'header',
        component: require('./components/common/header-page.vue')
      }
    })

    router.redirect({
        '/': '/default'
    })
    //注册路由切换前
    router.beforeEach(function (transition) {
        transition.next();

    });

    //注册路由切换后
    router.afterEach(function (transition) {
        //console.log('成功浏览到: ' + transition.to.path)
    });
}
